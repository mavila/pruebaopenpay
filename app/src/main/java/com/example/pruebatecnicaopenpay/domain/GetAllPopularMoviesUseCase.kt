package com.example.pruebatecnicaopenpay.domain

import com.example.pruebatecnicaopenpay.data.TMDBRepository
import com.example.pruebatecnicaopenpay.data.database.entities.toDatabase
import com.example.pruebatecnicaopenpay.domain.model.PopularMovies
import javax.inject.Inject

class GetAllPopularMoviesUseCase @Inject constructor(private val repository: TMDBRepository) {
    suspend operator fun invoke():List<PopularMovies>{
        val getPopularMovies = repository.getAllPopularMoviesApi()

        return if(getPopularMovies.isNotEmpty()){
            repository.clearPopularMovies()
            repository.insertPopularMovies(getPopularMovies.map { it.toDatabase() })
            getPopularMovies
        }else{
            repository.getAllPopularMoviesFromDataBase()
        }
    }
}