package com.example.pruebatecnicaopenpay.ui.fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pruebatecnicaopenpay.databinding.ProfileFragmentBinding
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies
import com.example.pruebatecnicaopenpay.ui.adapter.ProfileMoviesAdapter
import com.example.pruebatecnicaopenpay.ui.viewmodel.ProfileViewModel

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProfileFragment : Fragment() {

    private val profileViewModel: ProfileViewModel by viewModels()
    private val topRatedMovies:MutableList<TopRatedMovies> = ArrayList()
    private lateinit var mRecyclerView : RecyclerView
    private val profileMoviesAdapter : ProfileMoviesAdapter = ProfileMoviesAdapter()

    companion object {
        fun newInstance(): ProfileFragment = ProfileFragment()
    }
    private var _binding: ProfileFragmentBinding? = null

    private val binding get() = _binding!!
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        _binding = ProfileFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
    }

    private fun initViewModel(){

        profileViewModel.topRatedMovies.observe(viewLifecycleOwner){
            topRatedMovies.addAll(it)
            setUpRecyclerView(topRatedMovies)
        }
        profileViewModel.isLoading.observe(viewLifecycleOwner) {
            binding.loadingProfile.isVisible = it
        }
        profileViewModel.getAllTopRatedMovies()
    }
    @SuppressLint("NotifyDataSetChanged")
    private fun setUpRecyclerView(topRatedMovies:MutableList<TopRatedMovies>?){
        mRecyclerView = binding.rvListMoviesProfile
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        mRecyclerView.adapter = profileMoviesAdapter
        profileMoviesAdapter.submitList(topRatedMovies)
        mRecyclerView.adapter?.notifyDataSetChanged()
    }

}