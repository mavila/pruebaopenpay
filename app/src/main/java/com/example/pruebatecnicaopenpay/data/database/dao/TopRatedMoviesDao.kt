package com.example.pruebatecnicaopenpay.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pruebatecnicaopenpay.data.database.entities.TopRatedMoviesEntity

@Dao
interface TopRatedMoviesDao {

    @Query("SELECT * FROM top_rated_movies_table ORDER BY id DESC")
    suspend fun getTopRatedDao():List<TopRatedMoviesEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTopRatedMovies(students:List<TopRatedMoviesEntity>)

    @Query("DELETE FROM top_rated_movies_table")
    suspend fun deleteAllTopRatedMovies()

}