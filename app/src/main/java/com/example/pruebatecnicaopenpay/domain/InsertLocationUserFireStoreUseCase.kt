package com.example.pruebatecnicaopenpay.domain

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import javax.inject.Inject

class InsertLocationUserFireStoreUseCase @Inject constructor() {
    private val db = Firebase.firestore
    operator fun invoke(lat: String, lon: String, fecha : String):Boolean{
        val locationUser = hashMapOf(
            "latitud" to lat,
            "longitud" to lon,
            "fecha" to fecha
        )
        db.collection("location_user")
            .add(locationUser)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "DocumentSnapshotDocumentSnapshot added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { e ->
                Log.w(TAG, "Error adding document", e)

            }
        return true
    }
}