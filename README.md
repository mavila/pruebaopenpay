# PruebaTecnicaOpenPay

Prueba Open Pay

Splash
Inicio de la app.

-Perfil
Información de usuario y películas favoritas.

-Películas
Lista de películas seccionado por recomendaciones, populares y top rated.

-Mapa
Mapa donde visualiza al usuario, asi como pines donde ha registrado al usuario por partes del Mapa.

-Imágenes
Permite subir imágenes a Firebase Storage.

-Notificaciones
Notificaciones de imágenes y mapas.

-Extras
Room para DB.
Api retrofit para servicios.
Geolocalización.

## Mejoras

-Perfil
Se debe mejorar el diseño y la funcionalidad

-Películas
Agregar filtros de búsqueda y diseño.

-Mapas
Se agregarían controles en los mapas, además de mejorar el diseño de pines.

-Imágenes
Se mejoraría agregar más imágenes de selección para subir más de 1 imagen, además de mejor interfaz.

-Notificaciones
Se mejoraría interfaz gráfica de notificaciones, además de control de background y foreground.

-Extras
Mejor control de errores y excepciones.

## Captura de pantallas
Movil

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image1.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image2.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image3.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image4.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image5.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image6.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image7.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image8.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image9.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image10.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image11.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image12.png)

Firebase Console

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image13.png)

![](https://gitlab.com/mavila/pruebaopenpay/-/raw/developer/image14.png)