package com.example.pruebatecnicaopenpay.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.pruebatecnicaopenpay.R
import com.example.pruebatecnicaopenpay.domain.model.RecomendationMovies
import com.squareup.picasso.Picasso
import kotlin.Int
class RecomendationMoviesAdapter :
    ListAdapter<RecomendationMovies, RecomendationMoviesAdapter.ViewHolder>(DiffCallback()){

    private class DiffCallback : DiffUtil.ItemCallback<RecomendationMovies>() {
        override fun areItemsTheSame(oldItem: RecomendationMovies, newItem: RecomendationMovies): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: RecomendationMovies, newItem: RecomendationMovies): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.adapter_movies, parent, false))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currentList[position]
        holder.bind(item)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val image = view.findViewById(R.id.ivMovie) as ImageView
        private val name = view.findViewById(R.id.tvNameMoview) as TextView
        private val description = view.findViewById(R.id.tvDescriptionMovie) as TextView

        fun bind(recomendationMovies: RecomendationMovies){
            Picasso.get().load("https://image.tmdb.org/t/p/original"+recomendationMovies.posterPath).into(image)
            name.text = recomendationMovies.title
            description.text = recomendationMovies.overview
        }
    }
}