package com.example.pruebatecnicaopenpay.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.pruebatecnicaopenpay.R
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies
import com.squareup.picasso.Picasso
import kotlin.Int

class TopRatedMoviesAdapter() :
ListAdapter<TopRatedMovies, TopRatedMoviesAdapter.ViewHolder>(DiffCallback()){

    private class DiffCallback : DiffUtil.ItemCallback<TopRatedMovies>() {
        override fun areItemsTheSame(oldItem: TopRatedMovies, newItem: TopRatedMovies): Boolean {
            return oldItem.title == newItem.title
        }

        override fun areContentsTheSame(oldItem: TopRatedMovies, newItem: TopRatedMovies): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.adapter_movies, parent, false))
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = currentList[position]
        holder.bind(item)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val image = view.findViewById(R.id.ivMovie) as ImageView
        private val name = view.findViewById(R.id.tvNameMoview) as TextView
        private val description = view.findViewById(R.id.tvDescriptionMovie) as TextView

        fun bind(popularMovies: TopRatedMovies){
            Picasso.get().load("https://image.tmdb.org/t/p/original"+popularMovies.posterPath).into(image)
            name.text = popularMovies.title
            description.text = popularMovies.overview
        }
    }
}