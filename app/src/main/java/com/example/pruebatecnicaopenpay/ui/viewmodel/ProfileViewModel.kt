package com.example.pruebatecnicaopenpay.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pruebatecnicaopenpay.domain.GetAllPopularMoviesUseCase
import com.example.pruebatecnicaopenpay.domain.GetAllRecomendationMoviesUseCase
import com.example.pruebatecnicaopenpay.domain.GetAllTopRatedMoviesUseCase
import com.example.pruebatecnicaopenpay.domain.model.PopularMovies
import com.example.pruebatecnicaopenpay.domain.model.RecomendationMovies
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val getAllTopRatedMoviesUseCase: GetAllTopRatedMoviesUseCase
) : ViewModel() {

    val topRatedMovies = MutableLiveData<List<TopRatedMovies>>()
    val isLoading = MutableLiveData<Boolean>()


    fun getAllTopRatedMovies() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val topRatedMoviesUse = getAllTopRatedMoviesUseCase()
            topRatedMoviesUse.let {
                topRatedMovies.value = it
            }
            isLoading.postValue(false)
        }
    }
}