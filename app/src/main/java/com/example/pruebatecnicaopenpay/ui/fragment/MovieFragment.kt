package com.example.pruebatecnicaopenpay.ui.fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pruebatecnicaopenpay.R
import com.example.pruebatecnicaopenpay.databinding.MovieFragmentBinding
import com.example.pruebatecnicaopenpay.domain.model.PopularMovies
import com.example.pruebatecnicaopenpay.domain.model.RecomendationMovies
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies
import com.example.pruebatecnicaopenpay.ui.adapter.PopularMoviesAdapter
import com.example.pruebatecnicaopenpay.ui.adapter.RecomendationMoviesAdapter
import com.example.pruebatecnicaopenpay.ui.adapter.TopRatedMoviesAdapter
import com.example.pruebatecnicaopenpay.ui.viewmodel.MovieViewModel

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieFragment : Fragment() {

    private val movieViewModel: MovieViewModel by viewModels()
    private val recomendationMovies:MutableList<RecomendationMovies> = ArrayList()
    private val popularMovies:MutableList<PopularMovies> = ArrayList()
    private val topRatedMovies:MutableList<TopRatedMovies> = ArrayList()
    private lateinit var mRecyclerView : RecyclerView
    private val recomendationMoviesAdapter : RecomendationMoviesAdapter = RecomendationMoviesAdapter()
    private val popularMoviesAdapter : PopularMoviesAdapter = PopularMoviesAdapter()
    private val topRatedMoviesAdapter : TopRatedMoviesAdapter = TopRatedMoviesAdapter()
    companion object {
        fun newInstance(): MovieFragment = MovieFragment()
    }
    private var _binding: MovieFragmentBinding? = null

    private val binding get() = _binding!!
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = MovieFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initNavigationBottom()
        movieViewModel.getAllRecomendationMovies()
    }

    private fun initViewModel(){
        movieViewModel.recomendationMovies.observe(viewLifecycleOwner){
            recomendationMovies.addAll(it)
            setUpRecyclerView(1,recomendationMovies,null,null)
        }
        movieViewModel.popularMovies.observe(viewLifecycleOwner){
            popularMovies.addAll(it)
            setUpRecyclerView(2,null,popularMovies,null)
        }
        movieViewModel.topRatedMovies.observe(viewLifecycleOwner){
            topRatedMovies.addAll(it)
            setUpRecyclerView(3,null,null,topRatedMovies)
        }
        movieViewModel.isLoading.observe(viewLifecycleOwner) {
            binding.loadingMovie.isVisible = it
        }

    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setUpRecyclerView(section: Int, recomendationMovies:MutableList<RecomendationMovies>?,
                                  popularMovies: MutableList<PopularMovies>?, topRatedMovies:MutableList<TopRatedMovies>?){
        mRecyclerView = binding.rvListMovies
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(context)
        mRecyclerView.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        if(section == 1){
            mRecyclerView.adapter = recomendationMoviesAdapter
            recomendationMoviesAdapter.submitList(recomendationMovies)
        }else if(section == 2){
            mRecyclerView.adapter = popularMoviesAdapter
            popularMoviesAdapter.submitList(popularMovies)
        }else{
            mRecyclerView.adapter = topRatedMoviesAdapter
            topRatedMoviesAdapter.submitList(topRatedMovies)
        }
        mRecyclerView.adapter?.notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initNavigationBottom() {
        binding.movNavView.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_profile -> {
                    recomendationMovies.clear()
                    mRecyclerView.adapter?.notifyDataSetChanged()
                    movieViewModel.getAllRecomendationMovies()
                    true
                }
                R.id.action_movies -> {
                    popularMovies.clear()
                    mRecyclerView.adapter?.notifyDataSetChanged()
                    movieViewModel.getAllPopularMovies()
                    true
                }
                R.id.action_map -> {
                    topRatedMovies.clear()
                    mRecyclerView.adapter?.notifyDataSetChanged()
                    movieViewModel.getAllTopRatedMovies()
                    true
                }
                else -> false
            }
        }
    }
}