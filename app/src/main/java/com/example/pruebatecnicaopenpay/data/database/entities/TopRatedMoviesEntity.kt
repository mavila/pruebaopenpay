package com.example.pruebatecnicaopenpay.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies

@Entity(tableName = "top_rated_movies_table")
data class TopRatedMoviesEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "adult") val adult: Boolean,
    @ColumnInfo(name = "backdrop_path") val backdropPath: String,
    @ColumnInfo(name = "genre_ids") val genreIds: Int = 0,
    @ColumnInfo(name = "id") val id: Int = 0,
    @ColumnInfo(name = "original_language") val originalLanguage: String,
    @ColumnInfo(name = "original_title") val originalTitle: String,
    @ColumnInfo(name = "overview") val overview: String,
    @ColumnInfo(name = "popularity") val popularity: Double,
    @ColumnInfo(name = "poster_path") val posterPath: String,
    @ColumnInfo(name = "release_date") val releaseDate: String,
    @ColumnInfo(name = "title") val title: String,
    @ColumnInfo(name = "video") val video: Boolean,
    @ColumnInfo(name = "vote_average") val voteAverage: Double,
    @ColumnInfo(name = "vote_count") val voteCount: Int
)

fun TopRatedMovies.toDatabase() = TopRatedMoviesEntity(adult = adult, backdropPath =  backdropPath,genreIds = genreIds,  id = id, originalLanguage = originalLanguage,
    originalTitle = originalTitle, overview = overview, popularity = popularity, posterPath = posterPath, releaseDate = releaseDate,
    title = title, video = video, voteAverage = voteAverage, voteCount = voteCount)