package com.example.pruebatecnicaopenpay.data.model

import com.google.gson.annotations.SerializedName

data class PopularMovieModel(
    @SerializedName("page") val page: String,
    @SerializedName("results") val results: List<ResultPopularMovieModel>,
    @SerializedName("total_pages") val totalPages: String,
    @SerializedName("total_results") val totalResults: String
)