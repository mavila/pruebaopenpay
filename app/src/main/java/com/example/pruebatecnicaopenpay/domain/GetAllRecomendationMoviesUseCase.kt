package com.example.pruebatecnicaopenpay.domain

import com.example.pruebatecnicaopenpay.data.TMDBRepository
import com.example.pruebatecnicaopenpay.data.database.entities.toDatabase
import com.example.pruebatecnicaopenpay.domain.model.RecomendationMovies
import javax.inject.Inject

class GetAllRecomendationMoviesUseCase @Inject constructor(private val repository: TMDBRepository) {
    suspend operator fun invoke():List<RecomendationMovies>{
        val getRecomendationMovies = repository.getAllRecomendationsMoviesApi()

        return if(getRecomendationMovies.isNotEmpty()){
            repository.clearRecomendationMovies()
            repository.insertRecomendationMovies(getRecomendationMovies.map { it.toDatabase() })
            getRecomendationMovies
        }else{
            repository.getAllRecomendationsMoviesFromDataBase()
        }
    }
}