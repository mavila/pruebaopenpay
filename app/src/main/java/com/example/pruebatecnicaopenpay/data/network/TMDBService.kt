package com.example.pruebatecnicaopenpay.data.network

import com.example.pruebatecnicaopenpay.data.model.PopularMovieModel
import com.example.pruebatecnicaopenpay.data.model.RecomendationMovieModel
import com.example.pruebatecnicaopenpay.data.model.TopRatedMovieModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class TMDBService @Inject constructor(private val api: TMDBApiClient) {

    suspend fun getRecomendationsMovies(): RecomendationMovieModel {
        return withContext(Dispatchers.IO) {
            val response = api.getRecomendationsMovies()
            response.body()!!
        }
    }
    suspend fun getPopularMovies(): PopularMovieModel {
        return withContext(Dispatchers.IO) {
            val response = api.getPopularMovies()
            response.body()!!
        }
    }
    suspend fun getTopRatedMovies(): TopRatedMovieModel {
        return withContext(Dispatchers.IO) {
            val response = api.getTopRatedMovies()
            response.body()!!
        }
    }

}