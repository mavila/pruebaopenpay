package com.example.pruebatecnicaopenpay.ui.view


import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.fragment.app.Fragment
import com.example.pruebatecnicaopenpay.R
import com.example.pruebatecnicaopenpay.databinding.ActivityMainBinding
import com.example.pruebatecnicaopenpay.ui.fragment.ImageFragment
import com.example.pruebatecnicaopenpay.ui.fragment.MapFragment
import com.example.pruebatecnicaopenpay.ui.fragment.MovieFragment
import com.example.pruebatecnicaopenpay.ui.fragment.ProfileFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initNavigationBottom()
        val fragment = ProfileFragment.newInstance()
        openFragment(fragment)
    }

    private fun initNavigationBottom() {
        binding.botNavView.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.action_profile -> {
                    val fragment = ProfileFragment.newInstance()
                    openFragment(fragment)
                    true
                }
                R.id.action_movies -> {
                    val fragment = MovieFragment.newInstance()
                    openFragment(fragment)
                    true
                }
                R.id.action_map -> {
                    val fragment = MapFragment.newInstance()
                    openFragment(fragment)
                    true
                }
                R.id.action_image -> {
                    val fragment = ImageFragment.newInstance()
                    openFragment(fragment)
                    true
                }
                else -> false
            }
        }
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.main_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }



}