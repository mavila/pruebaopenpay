package com.example.pruebatecnicaopenpay

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PruebaTecnicaOpenPayApp:Application()