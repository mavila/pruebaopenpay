package com.example.pruebatecnicaopenpay.domain.model

import com.example.pruebatecnicaopenpay.data.database.entities.RecomendationMoviesEntity
import com.example.pruebatecnicaopenpay.data.model.ResultRecomendationMovieModel

data class RecomendationMovies (val adult:Boolean, val backdropPath:String,val genreIds : Int ,
                   val  id: Int, val originalLanguage: String, val originalTitle: String,
                   val overview: String, val popularity: Double, val posterPath: String,
                   val releaseDate: String, val title: String, val video: Boolean,
                   val voteAverage: Double, val voteCount: Int)

fun ResultRecomendationMovieModel.toDomain() = RecomendationMovies(adult, backdropPath,genreIds[0],id,originalLanguage,originalTitle,overview,popularity,
        posterPath, releaseDate,title ,video, voteAverage, voteCount)
fun RecomendationMoviesEntity.toDomain() = RecomendationMovies(adult, backdropPath,genreIds,id,originalLanguage,originalTitle,overview,popularity,
    posterPath, releaseDate,title ,video, voteAverage, voteCount)
