package com.example.pruebatecnicaopenpay.domain

import com.example.pruebatecnicaopenpay.data.TMDBRepository
import com.example.pruebatecnicaopenpay.data.database.entities.toDatabase
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies
import javax.inject.Inject

class GetAllTopRatedMoviesUseCase @Inject constructor(private val repository: TMDBRepository) {
    suspend operator fun invoke():List<TopRatedMovies>{
        val getTopRatedMovies = repository.getAllTopRatedMoviesApi()

        return if(getTopRatedMovies.isNotEmpty()){
            repository.clearTopRatedMovies()
            repository.insertTopRatedMovies(getTopRatedMovies.map { it.toDatabase() })
            getTopRatedMovies
        }else{
            repository.getAllTopRatedMoviesFromDataBase()
        }
    }
}