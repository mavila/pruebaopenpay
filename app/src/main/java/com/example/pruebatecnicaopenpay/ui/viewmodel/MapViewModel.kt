package com.example.pruebatecnicaopenpay.ui.viewmodel

import android.content.ContentValues
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pruebatecnicaopenpay.data.model.LocationUserFireStoreModel
import com.example.pruebatecnicaopenpay.domain.InsertLocationUserFireStoreUseCase
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapViewModel @Inject constructor(
    val insertLocationUserFireStoreUseCase : InsertLocationUserFireStoreUseCase,
    private val notificationBuilder: NotificationCompat.Builder,
    private val notificationManager: NotificationManagerCompat
) : ViewModel() {

    val isLoading = MutableLiveData<Boolean>()
    var flagInsertLocationUser =  MutableLiveData<Boolean>()
    val locationUserListFireStoreModel = MutableLiveData<List<LocationUserFireStoreModel>>()
    val listLocationFire = ArrayList<LocationUserFireStoreModel>()

    private val db = Firebase.firestore

    fun registerLocationUser(lat: String, lon: String, fecha: String) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val flag = insertLocationUserFireStoreUseCase(lat,lon,fecha)
            flag.let {
                flagInsertLocationUser.value = it
            }
            showSimpleNotification()
            isLoading.postValue(false)
        }
    }


    fun getAllLocationUser() {
        viewModelScope.launch {
            isLoading.postValue(true)
            db.collection("location_user")
                .get()
                .addOnSuccessListener { result ->
                    for (document in result) {
                        val locationUserFireStoreModel = LocationUserFireStoreModel(
                            document.data["latitud"].toString(),
                            document.data["longitud"].toString(),
                            document.data["fecha"].toString())
                        listLocationFire.add(locationUserFireStoreModel)
                    }
                    isLoading.postValue(false)
                    locationUserListFireStoreModel.value = listLocationFire
                }
                .addOnFailureListener { exception ->
                    isLoading.postValue(false)
                }
        }
    }

    private fun showSimpleNotification() {
        notificationManager.notify(1, notificationBuilder
            .setContentTitle("Ubicación  Registrada")
            .setContentText("Tu ubicación se registró exitosamente")
            .build()
        )


    }
}