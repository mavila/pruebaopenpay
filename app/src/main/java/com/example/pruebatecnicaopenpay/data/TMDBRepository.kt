package com.example.pruebatecnicaopenpay.data

import com.example.pruebatecnicaopenpay.data.database.dao.PopularMoviesDao
import com.example.pruebatecnicaopenpay.data.database.dao.RecomendationMoviesDao
import com.example.pruebatecnicaopenpay.data.database.dao.TopRatedMoviesDao
import com.example.pruebatecnicaopenpay.data.database.entities.PopularMoviesEntity
import com.example.pruebatecnicaopenpay.data.database.entities.RecomendationMoviesEntity
import com.example.pruebatecnicaopenpay.data.database.entities.TopRatedMoviesEntity
import com.example.pruebatecnicaopenpay.data.model.PopularMovieModel
import com.example.pruebatecnicaopenpay.data.model.RecomendationMovieModel
import com.example.pruebatecnicaopenpay.data.model.TopRatedMovieModel
import com.example.pruebatecnicaopenpay.data.network.TMDBService
import com.example.pruebatecnicaopenpay.domain.model.PopularMovies
import com.example.pruebatecnicaopenpay.domain.model.RecomendationMovies
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies
import com.example.pruebatecnicaopenpay.domain.model.toDomain
import javax.inject.Inject

class TMDBRepository @Inject constructor(
    private val api: TMDBService,private val recomendationMoviesDao: RecomendationMoviesDao, private val topRatedMoviesDao: TopRatedMoviesDao,
    private val popularMoviesDao: PopularMoviesDao) {

    suspend fun getAllRecomendationsMoviesApi():List<RecomendationMovies>  {
        val response: RecomendationMovieModel = api.getRecomendationsMovies()
        return response.results.map { it.toDomain() }
    }

    suspend fun getAllRecomendationsMoviesFromDataBase():List<RecomendationMovies>{
        val response: List<RecomendationMoviesEntity> = recomendationMoviesDao.getAllRecomendation()
        return response.map { it.toDomain() }
    }

    suspend fun getAllPopularMoviesApi():List<PopularMovies>  {
        val response: PopularMovieModel = api.getPopularMovies()
        return response.results.map { it.toDomain() }
    }

    suspend fun getAllPopularMoviesFromDataBase():List<PopularMovies>{
        val response: List<PopularMoviesEntity> = popularMoviesDao.getPopularnDao()
        return response.map { it.toDomain() }
    }

    suspend fun getAllTopRatedMoviesApi():List<TopRatedMovies>  {
        val response: TopRatedMovieModel = api.getTopRatedMovies()
        return response.results.map { it.toDomain() }
    }

    suspend fun getAllTopRatedMoviesFromDataBase():List<TopRatedMovies>{
        val response: List<TopRatedMoviesEntity> = topRatedMoviesDao.getTopRatedDao()
        return response.map { it.toDomain() }
    }


    suspend fun insertRecomendationMovies(recomendationMovies:List<RecomendationMoviesEntity>){
        recomendationMoviesDao.insertRecomendationMovies(recomendationMovies)
    }

    suspend fun insertPopularMovies(popularMovies:List<PopularMoviesEntity>){
        popularMoviesDao.insertPopulartMovies(popularMovies)
    }

    suspend fun insertTopRatedMovies(topRatedMovies:List<TopRatedMoviesEntity>){
        topRatedMoviesDao.insertTopRatedMovies(topRatedMovies)
    }

    suspend fun clearRecomendationMovies(){
        recomendationMoviesDao.deleteAllRecomendationMovies()
    }
    suspend fun clearPopularMovies(){
        popularMoviesDao.deleteAllPopularMovies()
    }
    suspend fun clearTopRatedMovies(){
        topRatedMoviesDao.deleteAllTopRatedMovies()
    }
}