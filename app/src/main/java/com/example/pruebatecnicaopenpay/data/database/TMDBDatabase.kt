package com.example.pruebatecnicaopenpay.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.pruebatecnicaopenpay.data.database.dao.PopularMoviesDao
import com.example.pruebatecnicaopenpay.data.database.dao.RecomendationMoviesDao
import com.example.pruebatecnicaopenpay.data.database.dao.TopRatedMoviesDao
import com.example.pruebatecnicaopenpay.data.database.entities.PopularMoviesEntity
import com.example.pruebatecnicaopenpay.data.database.entities.RecomendationMoviesEntity
import com.example.pruebatecnicaopenpay.data.database.entities.TopRatedMoviesEntity

@Database(entities = [PopularMoviesEntity::class,TopRatedMoviesEntity::class,RecomendationMoviesEntity::class], version = 1)
abstract class TMDBDatabase: RoomDatabase() {

    abstract fun getRecomendationDao(): RecomendationMoviesDao
    abstract fun getPopularDao(): PopularMoviesDao
    abstract fun getTopRatedDao(): TopRatedMoviesDao
}