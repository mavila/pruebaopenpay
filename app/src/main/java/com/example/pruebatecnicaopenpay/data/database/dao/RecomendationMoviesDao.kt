package com.example.pruebatecnicaopenpay.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pruebatecnicaopenpay.data.database.entities.RecomendationMoviesEntity

@Dao
interface RecomendationMoviesDao {

    @Query("SELECT * FROM recomendation_movies_table ORDER BY id DESC")
    suspend fun getAllRecomendation():List<RecomendationMoviesEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRecomendationMovies(students:List<RecomendationMoviesEntity>)

    @Query("DELETE FROM recomendation_movies_table")
    suspend fun deleteAllRecomendationMovies()

}