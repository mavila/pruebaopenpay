package com.example.pruebatecnicaopenpay.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.pruebatecnicaopenpay.data.database.entities.PopularMoviesEntity

@Dao
interface PopularMoviesDao {

    @Query("SELECT * FROM popular_movies_table ORDER BY id DESC")
    suspend fun getPopularnDao():List<PopularMoviesEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPopulartMovies(students:List<PopularMoviesEntity>)

    @Query("DELETE FROM popular_movies_table")
    suspend fun deleteAllPopularMovies()

}