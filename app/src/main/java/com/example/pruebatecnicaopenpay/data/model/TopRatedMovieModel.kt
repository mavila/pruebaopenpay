package com.example.pruebatecnicaopenpay.data.model

import com.google.gson.annotations.SerializedName

data class TopRatedMovieModel(
    @SerializedName("page") val page: String,
    @SerializedName("results") val results: List<ResultTopRatedMovieModel>,
    @SerializedName("total_pages") val totalPages: String,
    @SerializedName("total_results") val totalResults: String
)