package com.example.pruebatecnicaopenpay.ui.fragment


import android.Manifest
import android.annotation.SuppressLint
import android.content.IntentSender
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.pruebatecnicaopenpay.data.model.LocationUserFireStoreModel
import com.example.pruebatecnicaopenpay.databinding.MapFragmentBinding
import com.example.pruebatecnicaopenpay.ui.viewmodel.MapViewModel
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.Task
import dagger.hilt.android.AndroidEntryPoint
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date


@AndroidEntryPoint
class MapFragment : Fragment(), OnMapReadyCallback {

    private lateinit var map: GoogleMap
    private val SOLICITAR_ACCESS_FINE_LOCATION = 1000
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationCallback: LocationCallback
    private val mapViewModel: MapViewModel by viewModels()
    var handler: Handler = Handler()
    private val TIME = 300000
    companion object {
        fun newInstance(): MapFragment = MapFragment()
    }

    private var _binding: MapFragmentBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View {
        _binding = MapFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        iniGeo()
        mapViewModel.isLoading.observe(viewLifecycleOwner) {
            binding.loadingMap.isVisible = it
        }
        mapViewModel.locationUserListFireStoreModel.observe(viewLifecycleOwner) {
            createMarkers(it)
        }
        mapViewModel.flagInsertLocationUser.observe(viewLifecycleOwner) {
           if(it){
               mapViewModel.getAllLocationUser()
           }
        }
        mapViewModel.isLoading.postValue(true)
        createMapFragment()
    }

    private fun createMapFragment() {
        val mapFragment = childFragmentManager.findFragmentById(binding.mapFragment.id) as? SupportMapFragment
        mapFragment!!.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(21.678716311117768, -99.28697286706922), 2.0f))
    }

    private fun createMarker(lat : Double, lon : Double) {
        val favoritePlace = LatLng(lat,lon)
        moveCameraMap(favoritePlace)
    }

    private fun createMarkers(listMarkers : List<LocationUserFireStoreModel>) {
        for(market in listMarkers){
            val place = LatLng(market.latitud.toDouble(),market.longitud.toDouble())
            map.addMarker(MarkerOptions().position(place).title(market.fecha))
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode){
            SOLICITAR_ACCESS_FINE_LOCATION-> {
                if((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)){
                    addListener()
                } else{
                    val toast = Toast.makeText(requireContext(), "Permiso denegado", Toast.LENGTH_SHORT)
                    toast.show()
                }
                return
            }
        }
    }

    private fun checkPermission(cancel:()-> Unit, ok:() ->Unit){
        if(ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(requireActivity(),Manifest.permission.ACCESS_FINE_LOCATION)){
                cancel()
            } else{
                ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),SOLICITAR_ACCESS_FINE_LOCATION)
            }
        } else {
            val request = LocationRequest.create().apply {
                interval = 2000
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            }
            val builder = LocationSettingsRequest.Builder().addLocationRequest(request)
            val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
            val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
            task.addOnFailureListener {
                if (it is ResolvableApiException) {
                    try {
                        it.startResolutionForResult(requireActivity(), 12345)
                    } catch (_: IntentSender.SendIntentException) {
                    }
                }
            }.addOnSuccessListener {}
            ok()
        }
    }
    private fun showDialogPermission(){
        ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),SOLICITAR_ACCESS_FINE_LOCATION)
    }

    @SuppressLint("MissingPermission")
    fun addListener(){
        fusedLocationProviderClient.requestLocationUpdates(locationRequest,locationCallback, null)
    }

    override fun onPause() {
        super.onPause()
        removeLocationListener()

    }

    private fun removeLocationListener(){
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        ejecutarTarea()
    }

    override fun onResume() {
        super.onResume()
        checkPermission(cancel = {
            showDialogPermission()
        }, ok = {
            addListener()
        })
    }

    private fun iniGeo(){
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity())
        locationCallback = MyLocationCallBack()
        locationRequest = LocationRequest()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 5000
        locationRequest.fastestInterval = 5000
    }

    inner class MyLocationCallBack : LocationCallback(){
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            val location = locationResult.lastLocation
            location?.run {
                removeLocationListener()
                createMarker(latitude,longitude)
                val ahora = System.currentTimeMillis()
                val fecha = Date(ahora)
                val df: DateFormat = SimpleDateFormat("dd/MM/yy")
                val salida: String = df.format(fecha)
                mapViewModel.registerLocationUser(latitude.toString(),longitude.toString(),salida)
            }
        }
    }

    fun moveCameraMap(latLng :LatLng){
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(latLng, 13f),
            4000,
            null
        )
    }
    fun ejecutarTarea() {
        handler.postDelayed(object : Runnable {
            override fun run() {
                addListener()
                handler.postDelayed(this, TIME.toLong())
            }
        }, TIME.toLong())

    }
}