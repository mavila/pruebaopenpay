package com.example.pruebatecnicaopenpay.ui.viewmodel

import android.net.Uri
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ImageViewModel @Inject constructor(
    private val notificationBuilder: NotificationCompat.Builder,
    private val notificationManager: NotificationManagerCompat
) : ViewModel() {

    val isLoading = MutableLiveData<Boolean>()
    var uriImage =  MutableLiveData<Uri>()
    private val storageRef = Firebase.storage.reference;

    fun uploadImageFirebaseStorage(sd : String, imageUri : Uri) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val uploadTask = storageRef.child("file/$sd").putFile(imageUri)
            uploadTask.addOnSuccessListener {
                // using glide library to display the image
                storageRef.child("file/$sd").downloadUrl.addOnSuccessListener {
                    uriImage.value = it
                    showSimpleNotification()
                    isLoading.postValue(false)
                }.addOnFailureListener {
                    isLoading.postValue(false)
                }
            }.addOnFailureListener {
                isLoading.postValue(false)
            }
        }
    }

    private fun showSimpleNotification() {
        notificationManager.notify(1, notificationBuilder
            .setContentTitle("Imagen almacenada a Firebase")
            .setContentText("La imagen se cargó exitosamente a firebase")
            .build()
        )
    }
}