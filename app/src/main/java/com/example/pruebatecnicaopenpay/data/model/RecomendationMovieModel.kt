package com.example.pruebatecnicaopenpay.data.model

import com.google.gson.annotations.SerializedName

data class RecomendationMovieModel(
    @SerializedName("page") val page: String,
    @SerializedName("results") val results: List<ResultRecomendationMovieModel>,
    @SerializedName("total_pages") val totalPages: String,
    @SerializedName("total_results") val totalResults: String
)