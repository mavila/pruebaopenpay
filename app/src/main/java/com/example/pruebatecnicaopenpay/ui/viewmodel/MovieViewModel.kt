package com.example.pruebatecnicaopenpay.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pruebatecnicaopenpay.domain.GetAllPopularMoviesUseCase
import com.example.pruebatecnicaopenpay.domain.GetAllRecomendationMoviesUseCase
import com.example.pruebatecnicaopenpay.domain.GetAllTopRatedMoviesUseCase
import com.example.pruebatecnicaopenpay.domain.model.PopularMovies
import com.example.pruebatecnicaopenpay.domain.model.RecomendationMovies
import com.example.pruebatecnicaopenpay.domain.model.TopRatedMovies
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val getAllPopularMoviesUseCase: GetAllPopularMoviesUseCase,
    private val getAllRecomendationMoviesUseCase: GetAllRecomendationMoviesUseCase,
    private val getAllTopRatedMoviesUseCase: GetAllTopRatedMoviesUseCase
) : ViewModel() {

    val recomendationMovies = MutableLiveData<List<RecomendationMovies>>()
    val popularMovies = MutableLiveData<List<PopularMovies>>()
    val topRatedMovies = MutableLiveData<List<TopRatedMovies>>()
    val isLoading = MutableLiveData<Boolean>()

    fun getAllRecomendationMovies() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val recomendationMoviesUse = getAllRecomendationMoviesUseCase()
            recomendationMoviesUse.let {
                recomendationMovies.value = it
            }
            isLoading.postValue(false)
        }
    }

    fun getAllPopularMovies() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val popularMoviesUse = getAllPopularMoviesUseCase()
            popularMoviesUse.let {
                popularMovies.value = it
            }
            isLoading.postValue(false)
        }
    }

    fun getAllTopRatedMovies() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val topRatedMoviesUse = getAllTopRatedMoviesUseCase()
            topRatedMoviesUse.let {
                topRatedMovies.value = it
            }
            isLoading.postValue(false)
        }
    }
}