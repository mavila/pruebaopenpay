package com.example.pruebatecnicaopenpay.data.network

import com.example.pruebatecnicaopenpay.data.model.PopularMovieModel
import com.example.pruebatecnicaopenpay.data.model.RecomendationMovieModel
import com.example.pruebatecnicaopenpay.data.model.TopRatedMovieModel
import retrofit2.Response
import retrofit2.http.GET

interface TMDBApiClient {

    @GET("discover/movie?api_key=737366bad48dceec6a17432cea7ebb90")
    suspend fun getRecomendationsMovies(): Response<RecomendationMovieModel>

    @GET("movie/popular?api_key=737366bad48dceec6a17432cea7ebb90")
    suspend fun getPopularMovies(): Response<PopularMovieModel>

    @GET("movie/top_rated?api_key=737366bad48dceec6a17432cea7ebb90")
    suspend fun getTopRatedMovies(): Response<TopRatedMovieModel>


}