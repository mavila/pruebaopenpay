package com.example.pruebatecnicaopenpay.data.model

import com.google.gson.annotations.SerializedName

data class LocationUserFireStoreModel(
    @SerializedName("latitud") val latitud : String,
    @SerializedName("longitud") val longitud: String,
    @SerializedName("fecha") val fecha: String
)