package com.example.pruebatecnicaopenpay.ui.fragment


import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.OpenableColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.pruebatecnicaopenpay.databinding.ImageFragmentBinding
import com.example.pruebatecnicaopenpay.ui.viewmodel.ImageViewModel
import com.squareup.picasso.Picasso

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ImageFragment : Fragment() {

    private val imageViewModel: ImageViewModel by viewModels()
    companion object {
        fun newInstance(): ImageFragment = ImageFragment()
    }

    private var _binding: ImageFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        _binding = ImageFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imageViewModel.isLoading.postValue(false)
        binding.btnUploadImage.setOnClickListener{
            val galleryIntent = Intent(Intent.ACTION_PICK)
            galleryIntent.type = "image/*"
            imagePickerActivityResult.launch(galleryIntent)
        }
        imageViewModel.isLoading.observe(viewLifecycleOwner) {
            binding.loadingImage.isVisible = it
        }
        imageViewModel.uriImage.observe(viewLifecycleOwner) {
            Picasso.get().load(it).into(binding.imageView)
        }
    }

    private var imagePickerActivityResult: ActivityResultLauncher<Intent> =
        registerForActivityResult( ActivityResultContracts.StartActivityForResult()) { result ->
            if(result.resultCode != 0){
                if (result != null) {
                    val imageUri: Uri? = result.data?.data
                    val sd = getFileName(requireContext(), imageUri!!)
                    imageViewModel.uploadImageFirebaseStorage(sd!!,imageUri)
                }
            }
        }

    @SuppressLint("Range")
    private fun getFileName(context: Context, uri: Uri): String? {
        if (uri.scheme == "content") {
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            cursor.use {
                if (cursor != null) {
                    if(cursor.moveToFirst()) {
                        return cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                    }
                }
            }
        }
        return uri.path?.lastIndexOf('/')?.let { uri.path?.substring(it) }
    }

}