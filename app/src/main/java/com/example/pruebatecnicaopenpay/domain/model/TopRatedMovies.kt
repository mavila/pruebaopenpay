package com.example.pruebatecnicaopenpay.domain.model

import com.example.pruebatecnicaopenpay.data.database.entities.TopRatedMoviesEntity
import com.example.pruebatecnicaopenpay.data.model.ResultTopRatedMovieModel

data class TopRatedMovies (val adult:Boolean, val backdropPath:String,val genreIds : Int ,
                   val  id: Int, val originalLanguage: String, val originalTitle: String,
                   val overview: String, val popularity: Double, val posterPath: String,
                   val releaseDate: String, val title: String, val video: Boolean,
                   val voteAverage: Double, val voteCount: Int)

fun ResultTopRatedMovieModel.toDomain() = TopRatedMovies(adult, backdropPath,genreIds[0],id,originalLanguage,originalTitle,overview,popularity,
        posterPath, releaseDate,title ,video, voteAverage, voteCount)
fun TopRatedMoviesEntity.toDomain() = TopRatedMovies(adult, backdropPath,genreIds,id,originalLanguage,originalTitle,overview,popularity,
    posterPath, releaseDate,title ,video, voteAverage, voteCount)
